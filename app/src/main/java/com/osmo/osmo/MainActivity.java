package com.osmo.osmo;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.app.progresviews.ProgressWheel;
import com.scwang.wave.MultiWaveHeader;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.level_progress)
    ProgressWheel profileBadge;

    @BindView(R.id.mood_progress)
    RoundCornerProgressBar moodProgress;

    @BindView(R.id.food_progress)
    RoundCornerProgressBar foodProgress;

    @BindView(R.id.drink_progress)
    RoundCornerProgressBar drinkProgress;

    @BindView(R.id.water_element)
    MultiWaveHeader waterLevel;

    @BindView(R.id.money_status)
    TextView moneyStatus;

    @BindView(R.id.character_image)
    ImageView characterImage;

    @BindView(R.id.water_level_text)
    TextView waterLevelText;

    private float percentageOfWater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setWavePercentage(0.0f);
        setLevel(10);
        setExperience(10, 100);
        setDrinkProgress(0.2f);
        setMoodPercentage(0.3f);
        setFoodPercentage(0.5f);
        setMoneyStatus(100);
        setEmotChar(2);
        percentageOfWater = 0.0f;
        new CountDownTimer(11000,100) {

            @Override
            public void onTick(long l) {
                percentageOfWater += 1.0f;
                setWavePercentage(percentageOfWater);
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    @OnClick(R.id.statistics)
    void clickStatisticsButton() {
        // Fill Here
        Intent intent = new Intent(this, StatisticsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.store_button)
    void clickStoreButton() {
        // Fill here
    }

    @OnClick(R.id.progress_layout)
    void clickLevelLayout() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    private void setEmotChar(int condition) {
        if (condition == 0) {
            characterImage.setImageResource(R.drawable.main_page_character_low_01_01);
        } else if (condition == 1) {
            characterImage.setImageResource(R.drawable.main_page_character_med_01);
        } else {
            characterImage.setImageResource(R.drawable.main_page_character_high_01);
        }
    }

    private void setLevel(int level) {
        profileBadge.setStepCountText(String.valueOf(level));
    }

    private void setExperience(int currentExp, int maxExp) {
        int currentPercentage = currentExp * 360 / maxExp;
        profileBadge.setPercentage(currentPercentage);
    }

    private void setMoodPercentage(float percentage) {
        moodProgress.setMax(1.0f); // set this how much max percentage
        if (percentage <= 0.0f) {
            moodProgress.setProgress(0.0f);
            moodProgress.setSecondaryProgress(moodProgress.getMax());
        } else {
            moodProgress.setSecondaryProgress(0.0f);
            moodProgress.setProgress(percentage);
        }
    }

    private void setFoodPercentage(float percentage) {
        foodProgress.setMax(1.0f); // set this how much max percentage
        if (percentage <= 0.0f) {
            foodProgress.setProgress(0.0f);
            foodProgress.setSecondaryProgress(foodProgress.getMax());
        } else {
            foodProgress.setSecondaryProgress(0.0f);
            foodProgress.setProgress(percentage);
        }
    }

    private void setDrinkProgress(float percentage) {
        drinkProgress.setMax(1.0f); // set this how much max percentage
        if (percentage <= 0.0f) {
            drinkProgress.setProgress(0.0f);
            drinkProgress.setSecondaryProgress(drinkProgress.getMax());
        } else {
            drinkProgress.setSecondaryProgress(0.0f);
            drinkProgress.setProgress(percentage);
        }
    }

    private void setWavePercentage(float percentage) {
        float safePercentage = percentage;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        if (safePercentage < 0.0f) {
            safePercentage = 0.0f;
        } else if (safePercentage > 100.0f) {
            safePercentage = 100.0f;
        }
        float changedPercentage = (safePercentage / 20 * 9) + 25;
        float newHeightDp = pxToDp(height) / 100 * changedPercentage;
        int newHeightPx = dpToPx(Math.round(newHeightDp));
        waterLevel.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, newHeightPx));
        waterLevelText.setText(String.format(Locale.ENGLISH,"%d %%", Math.round(safePercentage)));
        /* Example of usage emot char */
        if (safePercentage >= 80.0f && safePercentage <= 100.0f) {
            setEmotChar(0);
        } else if (safePercentage >= 60.0f && safePercentage < 80.0f) {
            setEmotChar(1);
        } else {
            setEmotChar(2);
        }
    }

    private void setMoneyStatus(int money) {
        moneyStatus.setText(formatMoney(money));
    }

    private String formatMoney(int money) {
        if (money < 1000) {
            return String.valueOf(money);
        } else if (money < 1000000) {
            return String.format(Locale.ENGLISH, "%d K", money / 1000);
        } else {
            return String.format(Locale.ENGLISH, "%d M", money / 1000000);
        }
    }

    private int dpToPx(int dp) {
        float density = getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    private int pxToDp(int pixel) {
        float density = getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) pixel / density);
    }
}
